// stdlib functionality
#include <iostream>
// ROOT functionality
#include <TFile.h>
#include <TH1D.h>
// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"

#include "HDF5Utils/Writer.h"

struct JetWithSubjet {
  const xAOD::Jet* jet;
  const xAOD::Jet* subjet;
  const xAOD::BTagging* btag;
};

namespace H5Utils {
  template <size_t N, class I>
  Writer<N,I> makeWriter(
    H5::Group& group, const std::string& name,
    const Consumers<I>& consumers,
    const std::array<hsize_t, N>& extent = internal::uniform<N>(5),
    hsize_t batch_size = 2048) {
    return Writer<N,I>(group, name, consumers, extent, batch_size);
  }
}

int main(int narg, char** argv) {

  if (narg < 2) return 1;
  // initialize the xAOD EDM
  xAOD::Init();

  // open the input file
  TString inputFilePath = argv[1];
  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
  event.readFrom( iFile.get() );

  typedef SG::AuxElement AE;
  typedef ElementLink<xAOD::JetContainer> JetLink;
  AE::ConstAccessor<JetLink> acc_parent("Parent");

  std::string subjet_name = "GhostVR30Rmax4Rmin02TrackJetGhostTag";
  typedef std::vector<ElementLink<xAOD::IParticleContainer> > ParticleLinks;
  AE::ConstAccessor<ParticleLinks> subjet_acc(subjet_name);

  // this will be the output file
  H5::H5File hfile("jets.h5", H5F_ACC_TRUNC);

  // set up the subjet writer
  //
  // we'll use the standard lossless compression, in some applications
  // you can get away with HALF_PRECISION for floats
  H5Utils::Compression compression = H5Utils::Compression::HALF_PRECISION;
  //
  // create some "consumers": these are functions that eat jets
  H5Utils::Consumers<const JetWithSubjet&> consumers;
  consumers.add<float>(
    "ptRatio",
    [](const JetWithSubjet& j){ return j.subjet->pt() / j.jet->pt();},
    0,                          // default value
    compression);
  consumers.add<float>(
    "rnnip_pb",
    [a=AE::ConstAccessor<double>("rnnip_pb")](const JetWithSubjet& j) {
      return a(*j.btag);
    },
    0,                          // default value
    compression);
  consumers.add<float>(
    "JetFitter_mass",
    [a=AE::ConstAccessor<float>("JetFitter_mass")](const JetWithSubjet& j) {
      return a(*j.btag);
    },
    0,                          // default value
    compression);
  auto writer = H5Utils::makeWriter<1>(hfile, "subjets", consumers);

  // set up the consumers for fat jet things
  AE::ConstAccessor<int> acc_nh("GhostHBosonsCount");
  AE::ConstAccessor<int> acc_nb("GhostBHadronsFinalCount");
  H5Utils::Consumers<const xAOD::Jet*> fat_consumers;
  // if everything is the same type we can use loops
  for (const std::string& pt: {
      "GhostHBosonsPt", "GhostBHadronsFinalPt"}) {
    fat_consumers.add<float>(
      pt,
      [a=AE::ConstAccessor<float>(pt), acc_parent](
        const xAOD::Jet* j){
        return a(**acc_parent(*j));
      },
      NAN);
  }
  fat_consumers.add<float>(
    "pt",
    [](const xAOD::Jet* j){ return j->pt();},
    NAN);
  auto fatwriter = H5Utils::makeWriter<0>(hfile, "jets", fat_consumers);

  // for counting events
  unsigned count = 0;

  // get the number of events in the file to loop over
  const Long64_t numEntries = event.getEntries();

  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {

    // Load the event
    event.getEntry( i );

    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    event.retrieve( ei, "EventInfo" );
    if (i % 1000 == 0) {
      std::cout << "Processing run # " << ei->runNumber()
                << ", event # " << ei->eventNumber() << std::endl;
    }

    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    event.retrieve(jets, "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets");

    // loop through all of the jets and make selections
    for(const xAOD::Jet* jet : *jets) {
      if (jet->pt() < 200e3) continue;
      const xAOD::Jet* parent = *acc_parent(*jet);
      if (acc_nb(*parent) < 2 || acc_nh(*parent) < 1) continue;
      const ParticleLinks subjets = subjet_acc(*parent);
      std::vector<JetWithSubjet> selected_jets;
      for (const auto& subjet_link: subjets) {
        const xAOD::IParticle* particle = *subjet_link;
        const xAOD::Jet* sj = dynamic_cast<const xAOD::Jet*>(particle);
        JetWithSubjet subjet_data{jet, sj, sj->btagging()};
        selected_jets.push_back(subjet_data);
      }
      writer.fill(selected_jets);
      fatwriter.fill(jet);
    }

    // counter for the number of events analyzed thus far
    count += 1;
  }

  // exit from the main function cleanly
  return 0;
}
